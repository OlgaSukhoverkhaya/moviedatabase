﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Models
{
    public interface IBaseModel
    {
       public int Id { get; set; }
    }
}
