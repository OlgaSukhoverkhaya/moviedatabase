﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Models
{
    public class MovieDescription : IBaseModel
    {
        public int Id { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }
    }
}
