﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Models
{
    public class Category : IBaseModel
    { 
        public int Id { get; set; }
        [MaxLength(300), MinLength(2)]
        public string Name { get; set; }
        public List<Movie> Movies { get; set; }
       
    }
}
