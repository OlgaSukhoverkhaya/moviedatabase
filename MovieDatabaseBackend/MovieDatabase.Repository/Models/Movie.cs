﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Models
{
    public class Movie : IBaseModel
    {
        public int Id { get; set; }
        [MaxLength(300), MinLength(2)]
        public string Title { get; set; }
        public int Year { get;set; }
        public int DescriptionId { get; set; }
        public int Rating { get; set; }
        public int CategoryId { get; set; }


        public Category Category { get; set; }
        public MovieDescription Description { get; set; }
    }
}
