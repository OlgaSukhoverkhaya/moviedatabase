﻿using MovieDatabase.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository
{
    public class Repository : IRepository
    {
        private List<Movie> dataSet = new TestData().GetAllMoviesData(); // Instead of DbSet.

        public List<Movie> GetAll()
        {
            return dataSet;
        }

        public Movie GetById(int id)
        {
          return dataSet.FirstOrDefault(movie => movie.Id == id);
        }
    }
}
