﻿using MovieDatabase.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository
{
    public class TestData
    {

        private List<Category> categories = new List<Category>()
        {
            new Category() { Id = 1, Name = "Drama" },
            new Category() { Id = 2, Name = "Action"},
            new Category() { Id = 3, Name = "Fantasy"}
        };

        private List<MovieDescription> movieDescriptions = new List<MovieDescription>() {
            new MovieDescription() {Id = 1, Description = "The Batman is a 2022 American superhero film based on the DC Comics character Batman. Produced by DC Films, 6th & Idaho, and Dylan Clark Productions."},
            new MovieDescription() {Id = 2, Description = "Evan Treborn suffers blackouts during significant events of his life. As he grows up, he finds a way to remember these lost memories and a supernatural way."},
            new MovieDescription(){Id = 3, Description = "Based on a popular video game franchise, Uncharted, a treasure hunt roller coaster follows explorers Nathan (Tom Holland) and Sully (Mark Wahlberg) as they embark on a globetrotting adventure to uncover undiscovered riches."}
        };

        public List<Movie> GetAllMoviesData()
        {
            return new List<Movie>()
            {
              new Movie(){
                Id = 1,
                Title="The Batman",
                DescriptionId = 1,
                Description = this.movieDescriptions[0],
                Year = 2022,
                Rating = 4,
                CategoryId = 2,
                Category = this.categories[1]
                },
              new Movie(){
                Id = 2,
                Title="Butterfly effect",
                DescriptionId = 2,
                Description = this.movieDescriptions[1],
                Year = 2004,
                Rating = 5,
                CategoryId = 1,
                Category = this.categories[0]
                },
              new Movie(){
                Id = 3,
                Title="Synopsis",
                DescriptionId = 3,
                Description = this.movieDescriptions[2],
                Year = 2022,
                Rating = 3,
                CategoryId = 1,
                Category = this.categories[0]
                }
            };
        }
    }
}
