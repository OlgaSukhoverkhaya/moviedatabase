﻿using MovieDatabase.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Services
{
    public interface IMovieService
    {
        public List<Movie> GetAllMovies();
        public Movie GetMovieDetailsById(int id);

        public List<string> GetMovieTitles();
    }
}
