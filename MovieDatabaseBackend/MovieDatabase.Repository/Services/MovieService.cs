﻿using MovieDatabase.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Repository.Services
{
    public class MovieService : IMovieService
    {
        private IRepository _repository;
        public MovieService(IRepository repository)
        {
            _repository = repository;
        }
        public List<Movie> GetAllMovies()
        {
            return _repository.GetAll();
        }

        public Movie GetMovieDetailsById(int id)
        {
            return _repository.GetById(id);
        }

        public List<string> GetMovieTitles()
        {
            return _repository.GetAll().Select(movie => movie.Title).ToList();
        }
    }
}
