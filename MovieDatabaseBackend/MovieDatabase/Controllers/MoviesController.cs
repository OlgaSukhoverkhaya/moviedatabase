﻿using Microsoft.AspNetCore.Mvc;
using MovieDatabase.ModelMappers;
using MovieDatabase.Repository.Models;
using MovieDatabase.Repository.Services;
using MovieDatabase.ViewModels;

namespace MovieDatabase.Controllers
{
    [Route("Movies")]
    [ApiController]
    public class MoviesController : Controller
    {
        private IMovieService _movieService;
        private ILogger _logger;
        private MovieMapper mapper = new MovieMapper();
        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAllMovies()
        {
            try
            {
                List<MovieViewModel> allMovies = _movieService
                    .GetAllMovies()
                    .Select(movie => mapper.MapMovieEntityToViewModel(movie))
                    .ToList();

                return Ok(allMovies);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occured at MoviesController, GetAllMovies(): {ex}");
                return BadRequest("Error.");
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetMovieDetails(int id)
        {
            try
            {
                Movie movieEntity = _movieService.GetMovieDetailsById(id);

                if (movieEntity != null)
                {
                    MovieViewModel movieViewModel = mapper.MapMovieEntityToViewModel(movieEntity);

                    return Ok(movieViewModel);
                }

                _logger.LogError($"MoviesController, GetMovieDetails(): Movie is not found in DB, Id: {id}");
                return BadRequest("Error. Movie is not found.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occured at MoviesController, GetMovieDetails(): {ex}");
                return BadRequest("Error.");
            }
        }

        [HttpGet]
        [Route("Titles")]
        public IActionResult GetMovieTitles()
        {
            try
            {
                List<string> titles = _movieService.GetMovieTitles();

                return Ok(titles);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occured at MoviesController, GetMovieTitles(): {ex}");
                return BadRequest("Error.");
            }
        }
    }
}
