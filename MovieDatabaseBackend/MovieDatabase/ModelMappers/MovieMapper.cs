﻿using MovieDatabase.Repository.Models;
using MovieDatabase.ViewModels;

namespace MovieDatabase.ModelMappers
{
    public class MovieMapper
    {
        public MovieViewModel MapMovieEntityToViewModel(Movie movieEntity)
        {
            MovieViewModel viewModel = new MovieViewModel()
            {
                Id = movieEntity.Id,
                Title = movieEntity.Title,
                Year = movieEntity.Year,
                DescriptionId = movieEntity.DescriptionId,
                DescriptionText = movieEntity.Description.Description,
                Rating = movieEntity.Rating,
                CategoryId = movieEntity.CategoryId,
                CategoryName = movieEntity.Category.Name
            };

            return viewModel;
        }
    }
}
