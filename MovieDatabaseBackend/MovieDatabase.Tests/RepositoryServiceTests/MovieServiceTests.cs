﻿using Moq;
using MovieDatabase.Repository;
using MovieDatabase.Repository.Models;
using MovieDatabase.Repository.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase.Tests.RepositoryServiceTests
{
    [TestFixture]
    internal class MovieServiceTests
    {
        private Mock<IRepository> _repositoryMock { get; set; }
        private MovieService _movieService;
        private List<Movie> _movies = new TestData().GetAllMoviesData();

        [SetUp]
        public void SetUp()
        {
            _repositoryMock = new Mock<IRepository>();
            _movieService = new MovieService(_repositoryMock.Object);

            _repositoryMock.Setup(repo => repo.GetAll()).Returns(_movies);
            _repositoryMock.Setup(repo => repo.GetById(1)).Returns(_movies.FirstOrDefault(m => m.Id == 1));
            _repositoryMock.Setup(repo => repo.GetById(2)).Returns(_movies.FirstOrDefault(m => m.Id == 2));
        }

        [Test]
        public void GetAllMovies_ReturnsExactMoviesCount()
        {
            Assert.That(_movieService.GetAllMovies(), Is.Not.Null);
            Assert.That(_movieService.GetAllMovies().Count(), Is.EqualTo(_movies.Count()));
        }

        [TestCase(1)]
        [TestCase(2)]
        public void GetMovieDetailsById_Value1Or2_ReturnsMovieById(int id)
        {
            Assert.That(_movieService.GetMovieDetailsById(id).Title, Is.EqualTo(_movies.FirstOrDefault(m => m.Id == id).Title));
        }

        [TestCase(0)]
        [TestCase(22)]
        public void GetMovieDetailsById_Value0Or23_ReturnsNull(int id)
        {
            Assert.That(_movieService.GetMovieDetailsById(id), Is.Null);
        }
    }
}
