import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MovieDetailsComponent } from './components/movie.details/movie.details.component';

const routes: Routes = [
  {
    path: 'Home', component: HomeComponent
  },
  {
    path: 'MovieDetails/:id', component: MovieDetailsComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

