import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { MoviesHttpService } from '../../http-services/movies/movies.http.service';
import { faFilm, faStar } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public searchTitle: string = "";
  public moviesList$: Observable<Movie[]> = this.httpMoviesService.getAllMovies();
  public movieTitles$: Observable<string[]> = this.httpMoviesService.getMovieTitles();
  faFilm = faFilm;
  faStar = faStar;

  constructor(private httpMoviesService: MoviesHttpService, private router: Router) { }

  ngOnInit(): void {
  }

  public openMovieDetails(id: number): void {
    this.router.navigate(['/MovieDetails', id]);
  }

  public filterMoviesByTitle() {
    console.log("Inserted ", this.searchTitle);

    this.moviesList$ = this.httpMoviesService.getAllMovies().pipe(
      map(items => {
        return items.filter((item: Movie) => {
          return item.title.includes(this.searchTitle)
        })
      }));
  }
}
