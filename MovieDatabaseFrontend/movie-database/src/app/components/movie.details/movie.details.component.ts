import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MoviesHttpService } from 'src/app/http-services/movies/movies.http.service';
import { Movie } from 'src/app/models/movie';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie.details',
  templateUrl: './movie.details.component.html',
  styleUrls: ['./movie.details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  public movie$: Observable<Movie> | undefined;
  faStar = faStar;

  constructor(
    private httpMoviesService: MoviesHttpService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.movie$ = this.httpMoviesService.getMovieById(Number(id));
  }
}
