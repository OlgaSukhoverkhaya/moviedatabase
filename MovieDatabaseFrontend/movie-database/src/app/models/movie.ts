export interface Movie {
id: number;
title: string;
year: number;
descriptionId: number;
descriptionText: string;
rating: number;
categoryId: number;
categoryName: string; 
}

