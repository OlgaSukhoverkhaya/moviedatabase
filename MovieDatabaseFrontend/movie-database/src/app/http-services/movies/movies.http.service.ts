import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from 'src/app/models/movie';

@Injectable({
  providedIn: 'root'
})
export class MoviesHttpService {
  private apiEndpoint = "https://localhost:7181/Movies";

  constructor(private http: HttpClient) {

  }

  getAllMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.apiEndpoint);
  }

  getMovieById(id: number): Observable<Movie> {
    return this.http.get<Movie>(`${this.apiEndpoint}/${id}`);
  }

  getMovieTitles(){
    return this.http.get<string[]>(`${this.apiEndpoint}/Titles`);
  }
}
