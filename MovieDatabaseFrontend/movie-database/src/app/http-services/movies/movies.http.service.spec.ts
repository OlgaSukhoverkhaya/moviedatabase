import { TestBed } from '@angular/core/testing';

import { Movies.HttpService } from './movies.http.service';

describe('Movies.HttpService', () => {
  let service: Movies.HttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Movies.HttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
